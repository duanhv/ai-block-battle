class Game
  attr_accessor :state, :action

  def initialize(current_state)
    @state = current_state
    @action = Command.new
  end

  def caculate_next_action
    #binding.pry
    field = @state.players.first.field
    case @state.this_piece_type
    when I
     @action.random(field, 3, 4, @state.this_piece_type)
    when J
      @action.random(field, 3, 3, @state.this_piece_type)
    when L
      @action.random(field, 3, 3, @state.this_piece_type)
    when O
      @action.random(field, 4, 2, @state.this_piece_type)
    when S
      @action.random(field, 3, 3, @state.this_piece_type)
    when T
      @action.random(field, 3, 3, @state.this_piece_type)
    when Z
      @action.random(field, 3, 3, @state.this_piece_type)
    end
  end
end
