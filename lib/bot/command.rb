class Command
  ACTIONS = %w(left right down turnleft turnright).freeze

  def random(field, s, len, type) #s: start
    #puts "turnright," + move(field, s, 6)
    cur_height = cal_heights(field)
    cur_diff = cal_diff(cur_height)
    max = cur_height.each_with_index.max
    lowest_idx = max[1]

    c_s = check_010(cur_diff, cur_height)
    c_00 = check_00(cur_diff)
    c_hole = check_hole(cur_diff)

    #binding.pry
    if type == S and c_s > -1

      puts move_s(field, cur_diff, cur_height, s, c_s)

    elsif type == Z and c_s > -1

      puts move_z(field, cur_diff, cur_height, s, c_s)

    elsif type == O and cur_diff.include?(0)
      puts move_o(field, cur_diff, cur_height, s)

    elsif (type == L or type == J) and (c_hole > -1 or c_00 > -1)
      
      if c_hole > -1
        puts move_l_2(field, cur_diff, cur_height, s, c_hole, type)
      elsif c_00 > -1
        puts move_l(field, cur_diff, cur_height, s, c_00)
      end

    elsif type == T and cur_diff.include?(1)

      puts move_t(field, cur_diff, cur_height, s)

    elsif check_case1(field, cur_height, lowest_idx, len) #check equal low

      if type == L and cur_diff.include?(0)
        puts "turnright," + move(field, s+1, cur_diff.index(0))
      else
        puts move(field, s, max[1]-len+1)
      end

    elsif len == 4 #check I to put into hole

      puts "turnleft," + move(field, s+1, lowest_idx)

    else
      puts move(field, s, max[1]-len+1)
    end
  end

  def move(field, s, e)
    offset = (s-e).abs
    return "" if offset == 0
    mv = s > e ? "left" : "right"
    ([mv] * offset).join(",")
  end

  def cal_height(field, col)
    (0..19).each do |i|
      return i-1 if field[i][col] == "2"
    end
    19
  end

  def cal_heights(field)
    arr = []
    (0..9).each do |i|
      arr << cal_height(field, i)
    end
    arr
  end

  def check_case1(field, cur_height, lowest_idx, len)
    if lowest_idx - len + 1 >= 0
       ((lowest_idx - len + 1)..lowest_idx-1).each do |i|
          return false if cur_height[i] != cur_height[i+1]
       end
       return true
    end
    false
  end

  def cal_diff(cur_height)
    diff = []
    (0..8).each do |i|
      diff << (cur_height[i+1] - cur_height[i]).abs
    end
    diff
  end

 def move_t(field, cur_diff, cur_height, s)
    res = ""
    idx = 0
    cur_diff.each_with_index do |i, index|
       if i == 1
          idx = index
          break
       end
    end

    cur_diff.each_with_index do |i, index|
       if i == 1
          idx = index if cur_height[index] > cur_height[idx]
       end
    end

    if cur_height[idx] > cur_height[idx+1]
      res = "turnright," + move(field, s+1, idx)
    else
      res = "turnleft," + move(field, s, idx)
    end
    res
  end


  def move_s(field, cur_diff, cur_height, s, idx)
    res = ""

    if cur_height[idx] > cur_height[idx+2]
      res = move(field, s, idx)
    else
      res = "turnleft," +  move(field, s, idx)
    end
    #binding.pry
    res
  end

  def move_z(field, cur_diff, cur_height, s, idx)
    res = ""

    if cur_height[idx] > cur_height[idx+2]
      res = "turnleft," + move(field, s+1, idx)
    else
      res = move(field, s, idx)
    end
    res
  end

  def move_o(field, cur_diff, cur_height, s)
    idx = cur_diff.index(0)

    cur_diff.each_with_index do |i, index|
      if i == 0
        if cur_height[index] > cur_height[idx]
           idx = index
        end
      end
    end

    move(field, s, idx)
  end

  def move_l(field, cur_diff, cur_height, s, c_00)
    idx = c_00

    cur_diff.each_with_index do |i, index|
      if i == 0 and i != idx and i < 9
        if cur_diff[i+1] == 0
          if cur_height[index] > cur_height[idx]
             idx = index
          end
        end
      end
    end

    move(field, s, idx)
  end

  def move_l_2(field, cur_diff, cur_height, s, c_hole, type)
    idx = c_hole

    cur_diff.each_with_index do |i, index|
      if i == 0 and i != idx and i < 9
        if cur_diff[i+1] == 0
          if cur_height[index] > cur_height[idx]
             #idx = index
          end
        end
      end
    end

    res = ""
    if cur_height[c_hole]  <  cur_height[c_hole+1]
       if type == L
          res = "turnleft," + move(field, s, idx)
       end
    else
       if type == J
          res = "turnright," + move(field, s+1, idx)
       end
    end

    res = move_l(field, cur_diff, cur_height, s, c_hole) if res == ""
    res
  end


  def check_010(cur_diff, cur_height)
    return false unless cur_diff.include?(0)
    
    idx = -1
    cur_diff.each_with_index do |i, index|
      if (i == 0) and (index + 2 < 9)
        if cur_diff[index+1] == 1 and cur_diff[index+2] == 0
          idx = index
          break
        end
      end
    end
    idx
  end

  def check_00(cur_diff)
    return false unless cur_diff.include?(0)
    
    idx = -1
    cur_diff.each_with_index do |i, index|
      if (i == 0) and index < 9
        if cur_diff[index+1] == 0
          idx = index
          break
        end
      end
    end
    idx
  end

  def check_hole(cur_diff)
    if cur_diff.include?(2) or cur_diff.include?(3)
      return cur_diff.index(2) unless cur_diff.index(2).nil?
      return cur_diff.index(3) unless cur_diff.index(3).nil?
    end
    -1
  end

end


